# Dotfiles 

My config files and themes that I use.

## Used with

OS: Arch Linux

Desktop: xmonad

Shell: Bash

## License

Copyright 2024 Shahar Wijesinghe <shahar@shahar.cc>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.


