#
# ~/.bash_profile
#

set -o history

export PATH="$PATH:$HOME/.local/bin"

export QT_QPA_PLATFORMTHEME=qt6ct

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi

#if [[ ! $WAYLAND_DISPLAY && $XDG_VTNR -eq 1 ]]; then
#  exec sway
#fi
