#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export XDG_MENU_PREFIX=arch- kybuildsycoca6
export EDITOR='nvim'

export HISTFILE
export HISTFILESIZE=999999
export HISTSIZE=999999

alias ls='ls --color=auto'
alias grep='grep --color=auto'

if [[ $TERM = 'st-256color' ]]; then
	transset-df "0.965" -a >/dev/null
fi

eval "$(starship init bash)"
