import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Layout.NoBorders (noBorders, smartBorders)
import XMonad.Layout.ToggleLayouts (ToggleLayout(..), toggleLayouts)
import XMonad.Layout.IM
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Reflect
import XMonad.Layout.Grid
import XMonad.Layout.Fullscreen
import XMonad.Prompt
import XMonad.Prompt.Man
import System.IO
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Spacing

myTerminal = "kitty" --terminal
myWorkspaces = ["1","2","3","4","5","6","7","8","9"] --list of tag names
myManageHook = composeAll [
--    [ className =? "vlc"        --> doFloat --float mplayer
--    , className =? "Gimp"           --> doShift ".42." --move gimp to window
--    , className =? "Keepassx"       --> doCenterFloat --float keepassx
--    , className =? "Firefox"        --> doShift "Panic!" --move firefox to window
--    , className =? "feh"            --> doCenterFloat --center and float feh
    ]
--myLayoutHook = onWorkspace ".42." gimp $ onWorkspace "Don't" terminalLayout $ onWorkspace "Panic!" webLayout $ standardLayout --per workspace layouts
myLayoutHook =  smartSpacing 6
		$ toggleLayouts (noBorders Full) (standardLayout)
    where
        standardLayout = avoidStruts ( tall ) --layout to use on every other workspace
            where
                tall = Tall nmaster delta ratio --define tall layout sizes
                nmaster = 1 --number of windows in master pane
                ratio = 1/2 --ratio of master pane size
                delta = 2/100

--        gimp =  avoidStruts $ --layout for gimp
--                withIM (0.11) (Role "gimp-toolbox") $ --toolbox on side
--                reflectHoriz $
--                withIM (0.15) (Role "gimp-dock") Full --dock on side
--        terminalLayout = avoidStruts $ Grid --layout for terminal windows
--        webLayout = avoidStruts $ Mirror tall --layout for browser and terminal window
--            where
--                tall = Tall nmaster delta ratio --define tall layout sizes 
--                nmaster = 1 --number of windows in master pane1
--                ratio = 3/4 --ratio of master pane size 
--                delta = 2/100
--xmobar config
myLogHook h = dynamicLogWithPP xmobarPP
            { ppHidden = xmobarColor "grey" "" --tag color
            , ppOutput = hPutStrLn h           --tag list and window title
            , ppTitle = xmobarColor "green" "" --window title color
            }
myStartupHook :: X ()
myStartupHook = do
            spawn "dunst" --start second xmobar

main = do 
--    din <- spawnPipe myStatusBar
    xmonad $ ewmh $ docks def
        { manageHook = manageDocks <+> myManageHook <+> manageHook def
        , layoutHook = myLayoutHook 
--        , logHook = myLogHook din
        , startupHook = myStartupHook
        , terminal = myTerminal
        , workspaces = myWorkspaces
        , modMask = mod4Mask
	, focusedBorderColor = "#a8afd5"
	, normalBorderColor = "#3F4150"
        } `additionalKeys`
        [ ((mod4Mask, xK_c),    spawn "alacritty") --starts terminal
        , ((mod4Mask, xK_r),  spawn "rofi -show combi drun run") --starts app launcher
        , ((mod4Mask, xK_p),    spawn "dmenu_run -nb black -nf white") --call dmenu
        , ((mod4Mask .|. shiftMask, xK_h), spawn "feh --scale ~/pictures/Xmbindings.png") --keymask dialog
	, ((mod4Mask, xK_f), sendMessage (Toggle "Full"))
        , ((0, xK_Print),       spawn "flameshot gui") --take screenshot
        ---Media Keys
        , ((0, 0x1008ff13),     spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%") --raise sound
        , ((0, 0x1008ff11),     spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%") --lower sound
        , ((0, 0x1008ff12),     spawn "amixer -q set Master toggle") --mute sound
        , ((0, 0x1008ff2c),     spawn "eject") --eject cd
        , ((mod4Mask, xK_q), kill) --close a window
        ---Power Options
	, ((mod4Mask .|. shiftMask, xK_End), spawn "systemctl poweroff")
	, ((mod4Mask .|. mod1Mask, xK_End), spawn "systemctl reboot")
	---Restart and recompile xmonad
	, ((mod4Mask .|. shiftMask, xK_r), spawn "xmonad --restart")
	, ((mod4Mask .|. shiftMask, xK_e), spawn "xmonad --recompile")
        ]
